package dk.kifi.test.optimistic.locking;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PreUpdate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.kifi.test.util.cdi.InjectionUtil;

public class CascadeOptimisticVersioningListener {
	private static final Logger logger = LoggerFactory.getLogger(CascadeOptimisticVersioningListener.class);

	@PreUpdate
	public void update(Composited compositedObject) {
		logger.info("Locking for {}", compositedObject);
		Object owner = compositedObject.getOwner();
		getEntityManager().lock(owner, LockModeType.OPTIMISTIC_FORCE_INCREMENT);
	}

	private EntityManager getEntityManager() {
		return InjectionUtil.getBeanReference(EntityManager.class);
	}
}
