package dk.kifi.test.optimistic.locking;

import java.util.concurrent.Future;

import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class TransactionControllerTest {
	@EJB OptimisticLockTest test;
	
	@Asynchronous
	public Future<Long> blockingUpdate(Long id, long sleepTime) {
		Long count = test.updateComment(id);
		sleep(sleepTime);
		return new AsyncResult<Long>(count);
	}
	
	private void sleep(long time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
		}
	}
}
