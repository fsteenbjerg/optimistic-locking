package dk.kifi.test.optimistic.locking;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.OptimisticLock;

@Entity
@Table(name = "opt_topic")
public class Topic {
	@Id
	@SequenceGenerator(name = "seq_gen", sequenceName = "id_seq", allocationSize = 20)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_gen")
	protected Long id;

	@Version
	@Column(nullable = false)
	private Long version;

	@OneToMany()
	@OptimisticLock(excluded=true)
	private List<Post> posts = new ArrayList<>();

	private String name;

	public Long getId() {
		return id;
	}

	public void addPost(Post post) {
		post.setTopic(this);
		posts.add(post);
	}

	public List<Post> getPosts() {
		return posts;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Topic [id=" + id + ", version=" + version + ", posts=" + posts + ", name=" + name + "]";
	}
}
