package dk.kifi.test.optimistic.locking;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
@LocalBean
public class OptimisticLockTest {
	private static final Logger logger = LoggerFactory.getLogger(OptimisticLockTest.class);
	private @PersistenceContext EntityManager mgr;

	public Topic createTopic() {
		Topic t = new Topic();
		t.setName("test");
		mgr.persist(t);
		return t;
	}
	
	public Post testCascadePersist(Topic author) {
		Topic t = mgr.merge(author);
		
		Post p = new Post();
		p.setTopic(t);
		t.addPost(p);
		p.setCount(1L);
		
		Comment c = new Comment();
		c.setCount(2L);
		
		p.addComment(c);
		c.setPost(p);
		
		c = new Comment();
		c.setCount(20L);
		
		p.addComment(c);
		c.setPost(p);

		mgr.persist(p);
		
		logger.info("Persisted: {}", p);
		return p;
	}
	
	public String printPost(Long id) {
		Post p = mgr.find(Post.class, id);
		logger.info("Printing Post : {}", p);
		return p.getTopic().toString();
	}
	
	public Long updateComment(Long id) {
		Comment c = mgr.find(Comment.class, id);
		c.setCount(c.getCount() + 1);
		return c.getCount();
	}

	public Post addComment(Long id) {
		Post p = mgr.find(Post.class, id);
		
		Comment c = new Comment();
		c.setCount(10L);
		
		p.addComment(c);
		
		return p;
	}
}
