package dk.kifi.test.optimistic.locking;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="opt_comment")
@EntityListeners(CascadeOptimisticVersioningListener.class)
public class Comment implements Composited {
	@Id
	@SequenceGenerator(
	    name="seq_gen",
	    sequenceName="id_seq",
	    allocationSize=20
	)
	@GeneratedValue(
		strategy=GenerationType.SEQUENCE, 
		generator="seq_gen"
	)
	protected Long id;

	@ManyToOne
    @JoinColumn(name = "post_id")
    private Post post;
    private Long count = 0L;
    
    @Override
    public Object getOwner() {
    	return getPost();
    }
    
    public Long getId() {
    	return id;
    }
    
    public void setPost(Post post) {
		this.post = post;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Post getPost() {
		return post;
	}

	@Override
	public String toString() {
		return "Comment [post=" + (post != null ? post.getId() : "N/A") + ", count=" + count + "]";
	}
}
