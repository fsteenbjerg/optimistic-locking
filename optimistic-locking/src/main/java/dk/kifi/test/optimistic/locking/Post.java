package dk.kifi.test.optimistic.locking;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "opt_post")
public class Post {
	@Id
	@SequenceGenerator(name = "seq_gen", sequenceName = "id_seq", allocationSize = 20)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_gen")
	protected Long id;

	@Version
	@Column(nullable = false)
	private Long version;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Comment> comments = new ArrayList<Comment>();

	@ManyToOne
    @JoinColumn(name = "topic_id")
    private Topic topic;

	private Long count = 0L;

	public Long getId() {
		return id;
	}

	public void addComment(Comment comment) {
		comment.setPost(this);
		comments.add(comment);
	}

	public List<Comment> getComments() {
		return comments;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Topic getTopic() {
		return topic;
	}
	
	public void setTopic(Topic topic) {
		this.topic = topic;
	}
	
	@Override
	public String toString() {
		return "Post [id=" + id + ", version=" + version + ", comments=" + comments + ", count=" + count + ", topic=" + topic.getId() + "]";
	}
}
