package dk.kifi.test.optimistic.locking;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
@WebServlet("/optimistic")
public class TestInvoker extends HttpServlet {
	@EJB
	OptimisticLockTest tester;
	@EJB
	TransactionControllerTest transactionTester;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try (PrintWriter out = response.getWriter()) {
			out.println("Normal Case:");
			testNormalCase(out);
			out.println("");
			out.println("Simultanous Case");
			testSimultanousCase(out);
		}
	}

	private void testSimultanousCase(PrintWriter out) {
		Topic a = tester.createTopic();

		Post p = tester.testCascadePersist(a);
		out.println("Created objects: " + tester.printPost(p.getId()));
		
		List<Future<Long>> result = new ArrayList<>();
		p.getComments().stream().forEach(c -> { 
			result.add(transactionTester.blockingUpdate(c.getId(), 3000));
		});
		
		result.stream().forEach(f -> getResult(out, f));
		
		out.println("Updated child object: " + tester.printPost(p.getId()));
	}
	
	private void getResult(PrintWriter out, Future<Long> f) {
		try {
			out.println("Result for update: " + f.get());
		} catch (Exception e) {
			out.println("Result for update: " + e.getMessage());
		}
	}

	private void testNormalCase(PrintWriter out) {
		Topic a = tester.createTopic();

		Post p = tester.testCascadePersist(a);
		out.println("Created objects: " + tester.printPost(p.getId()));
		
		p.getComments().stream().forEach(c -> tester.updateComment(c.getId()));
		out.println("Updated child object: " + tester.printPost(p.getId()));
		
		p = tester.addComment(p.getId());
		out.println("Added child object: " + tester.printPost(p.getId()));
	}
}
