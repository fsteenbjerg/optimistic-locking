package dk.kifi.test.optimistic.locking;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EntityManagerProducer {
	private static final Logger logger = LoggerFactory.getLogger(EntityManagerProducer.class);
    @PersistenceContext    
    private EntityManager em;

    @Produces
//    @RequestScoped
    public EntityManager getEntityManager() {
    	logger.info("Creating entity manager");
        return em;
    }
}