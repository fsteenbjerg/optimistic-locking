package dk.kifi.test.optimistic.locking;

public interface Composited {
	Object getOwner();
}
