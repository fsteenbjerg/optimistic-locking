package dk.kifi.test.util.cdi;

import java.util.Set;

import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.naming.InitialContext;

public class InjectionUtil {
	@SuppressWarnings("unchecked")
	public static <T> T getBeanReference(Class<T> type) {
		try {
			final InitialContext ctx = new InitialContext();
			final BeanManager manager = (BeanManager) ctx.lookup("java:comp/BeanManager");
			final Set<Bean<?>> beans = manager.getBeans(type);
			final Bean<?> bean = manager.resolve(beans);
			final CreationalContext<?> creationalContext = manager.createCreationalContext(bean);
			return (T) manager.getReference(bean, type, creationalContext);
		} catch (Exception e) {
			throw new RuntimeException("Failed to find bean", e);
		}
	}
}
